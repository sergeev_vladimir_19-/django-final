from django.shortcuts import render
from rest_framework import generics
from .serializers import *
from .models import *
class AuthorCreateView(generics.CreateAPIView):
    serializer_class=AuthorDetailSerializer
class BookCreateView(generics.CreateAPIView):
    serializer_class=BookDetailSerializer
class GenreCreateView(generics.CreateAPIView):
    serializer_class=GenreDetailSerializer

class BookListView(generics.ListAPIView):
    serializer_class=BookDetailSerializer
    queryset=Book.objects.all()
class BookDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class=BookDetailSerializer
    queryset=Book.objects.all()
# Create your views here.
