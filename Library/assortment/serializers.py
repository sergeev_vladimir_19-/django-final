from rest_framework import serializers
from .models import *


class AuthorDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Author
        fields='__all__'
class BookDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Book
        fields='__all__'
class GenreDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Genreofthebook
        fields='__all__'
class BookListSerializer(serializers.ModelSerializer):
    class Meta:
        model=Book
        fields='__all__'
