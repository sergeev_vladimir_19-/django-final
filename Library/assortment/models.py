from django.db import models
from django.utils import timezone
# Create your models here.
class Author(models.Model):
    title=models.CharField(verbose_name='Автор книги',max_length=100)
    count_composition=models.IntegerField()
    about_author=models.TextField()
    def __str__(self):
        return self.title

class Book(models.Model):
    title=models.CharField(verbose_name='Книга',max_length=100)
    about_book=models.TextField()
    count_book=models.IntegerField()
    more_volume=models.BooleanField(default=False)
    date_published=models.DateTimeField(default=timezone.now,null=True,blank=True)
    author=models.ForeignKey('Author',on_delete=models.CASCADE)
    genre=models.ForeignKey('Genreofthebook',on_delete=models.CASCADE)
    def __str__(self):
        return self.title
class Genreofthebook(models.Model):
    title=models.CharField(verbose_name='Жанр книги',max_length=100)
    for_children=models.BooleanField(default=False)
    genre_id=models.IntegerField()
    def __str__(self):
        return self.title
