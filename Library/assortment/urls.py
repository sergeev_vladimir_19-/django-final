from django.contrib import admin
from django.urls import path,include
from .views import *

app_name='assortment'
urlpatterns=[
    path('author/create/',AuthorCreateView.as_view()),
    path('book/create/',BookCreateView.as_view()),
    path('genre/create/',GenreCreateView.as_view()),
    path('list/',BookListView.as_view()),
    path('edit/<int:pk>/', BookDetailView.as_view())
]
